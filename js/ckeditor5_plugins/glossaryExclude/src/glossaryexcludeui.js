import { Plugin } from 'ckeditor5/src/core';
import { ButtonView } from 'ckeditor5/src/ui';
import icon from '../../../../assets/icons/nonomasticon.svg';

export default class Glossaryexcludeui extends Plugin {

  /**
   * @inheritdoc
   */
  static get requires() {
    return [ButtonView];
  }

  init() {
    const editor = this.editor;
    const t = editor.t;

    editor.ui.componentFactory.add('glossaryExclude', locale => {
      const command = editor.commands.get('nonomasticon');
      const view = new ButtonView(locale);

      view.set({
        label: t('Glossary exclude'),
        icon,
        tooltip: true,
        isToggleable: true,
      });

      view.bind('isOn', 'isEnabled').to(command, 'value', 'isEnabled');

      // Execute command.
      this.listenTo(view, 'execute', () => {
        editor.execute('nonomasticon');
        editor.editing.view.focus();
      });

      return view;
    });
  }
}
