import Glossaryexcludeediting from './glossaryexcludeediting';
import Glossaryexcludeui from './glossaryexcludeui';
import { Plugin } from 'ckeditor5/src/core';

class GlossaryExclude extends Plugin {

  static get requires() {
    return [Glossaryexcludeediting, Glossaryexcludeui];
  }

  /**
   * @inheritdoc
   */
  static get pluginName() {
    return 'GlossaryExclude';
  }

}

export default {
  GlossaryExclude,
};
