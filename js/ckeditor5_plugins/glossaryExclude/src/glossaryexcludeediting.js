import { Plugin } from 'ckeditor5/src/core';
import { Widget } from 'ckeditor5/src/widget';
import AttributeCommand from '@ckeditor/ckeditor5-basic-styles/src/attributecommand';

export default class Glossaryexcludeediting extends Plugin {
  static get requires() {
    return [Widget];
  }

  init() {
    this._defineSchema();
    this._defineConverters();
    this.editor.commands.add( 'nonomasticon', new AttributeCommand( this.editor, 'nonomasticon' ) );
  }

  _defineSchema() {
    const schema = this.editor.model.schema;
    schema.extend( '$text', { allowAttributes: 'nonomasticon' } );
    schema.setAttributeProperties('nonomasticon', {
      isFormatting: true,
      copyOnEnter: true
    } );
  }

  _defineConverters() {
    const { conversion } = this.editor;
    conversion.attributeToElement( {
      model: 'nonomasticon',
      view: 'nonomasticon',
    } );
  }
}
