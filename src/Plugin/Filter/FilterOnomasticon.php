<?php

namespace Drupal\onomasticon\Plugin\Filter;

use Drupal\Core\Config\Schema\ArrayElement;
use Drupal\filter\FilterProcessResult;
use Drupal\filter\Plugin\FilterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\taxonomy\Entity\Vocabulary;
use Drupal\taxonomy\Entity\Term;
use Masterminds\HTML5;

/**
 * @Filter(
 *   id = "filter_onomasticon",
 *   title = @Translation("Onomasticon Filter"),
 *   description = @Translation("Adds glossary information to words."),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_TRANSFORM_IRREVERSIBLE,
 *   settings = {
 *     "onomasticon_vocabulary" = "",
 *     "onomasticon_definition_field" = "description",
 *     "onomasticon_definition_filters" = false,
 *     "onomasticon_tag" = "dfn",
 *     "onomasticon_disabled" = "abbr audio button cite code dfn form meta object pre style script video",
 *     "onomasticon_implement" = "extra_element",
 *     "onomasticon_orientation" = "below",
 *     "onomasticon_cursor" = "default",
 *     "onomasticon_repetition" = "",
 *     "onomasticon_ignorecase" = false,
 *     "onomasticon_termlink" = false,
 *   },
 * )
 */
class FilterOnomasticon extends FilterBase {

  /**
   * @var HTML5 DOMDocument
   * This var is the base element all replacements are made on.
   */
  private $htmlDom;

  /**
   * @var array
   * Contains all paths of processed DOM Nodes to avoid
   * duplicate replacements.
   */
  private $processedPaths = [];

  /**
   * @var array
   * Collection of replacements. Gets applied to $htmlDom
   * one the complete tree has been traversed.
   */
  private $htmlReplacements = [];

  /**
   * @var Term[]
   * Simple cache mechanism for loaded terms. Also works
   * as a list of already processed terms.
   */
  private $terms = [];

  /**
   * @var array
   * Simple cache mechanism for loaded terms. Also works
   * as a list of already processed terms.
   */
  private $termCache = [];

  /**
   * Main filter function as expected by Drupal.
   *
   * @param string $text
   * @param string $langcode
   * @return \Drupal\filter\FilterProcessResult|string
   */
  public function process($text, $langcode) {
    // Check if a vocabulary has been set.
    if (empty($this->settings['onomasticon_vocabulary'])) {
      return $text;
    }

    // Load the description into an HTML5 DOMDocument.
    $html5 = new HTML5();
    $this->htmlDom = $html5->loadHTML('<html><body>' . $text . '</body></html>');
    $this->htmlDom->preserveWhiteSpace = false;
    // Normalize in case HTMLCorrector has not been run.
    $this->htmlDom->normalizeDocument();
    // Get the root element (<html>).
    $rootElement = $this->htmlDom->documentElement;
    // Get the body element (<body>).
    $body = $rootElement->getElementsByTagName('body')->item(0);
    // Walk the DOM and replace terms with definitions.
    $this->processChildren($body);
    // Traversing finished, let's replace child nodes.
    foreach ($this->htmlReplacements as $r) {
      $domFragment = $this->htmlDom->createDocumentFragment();
      // XML doesn't know some named entities like &nbsp;
      try {
        $bool = $domFragment->appendXML($this->htmlEntitiesNormalizeXml($r['html']));
      }
      catch (Exception $e) {
        $bool = FALSE;
      }
      if ($bool) {
        $r['dom']->parentNode->replaceChild($domFragment, $r['dom']);
      }
    }

    // Export all "body" childes as HTML 5.
    $text = '';
    foreach ($body->childNodes as $childNode) {
      $text .= $html5->saveHTML($childNode);
    }
    // Prepare return object.
    $result = new FilterProcessResult($text);
    // We force the cache to be cleared automatically on changes to the vocabulary.
    $result->addCacheTags([
      'taxonomy_term_list:' . $this->settings['onomasticon_vocabulary'],
    ]);
    $remove_onomasticon_tag = $this->removeCustomTag('nonomasticon', $text);
    if ($remove_onomasticon_tag) {
      $result->setProcessedText($remove_onomasticon_tag);
    }
    return $result;
  }

  /**
   * Remove custom tags added for editing purposes.
   *
   * @param string $tag
   *   The custom html tag.
   * @param string $text
   *   The text where should replace tag.
   *
   * @return string
   *   A processed text without given html tag.
   */
  private function removeCustomTag(string $tag, string $text): string {
    return preg_replace('/<\/?' . $tag . '[^>]*>/', '', $text);
  }

  /**
   * Unicode-proof htmlentities function.
   * Returns 'normal' chars as chars and special characters
   * as numeric html entities.
   *
   * @param string $string
   * @return string
   * */
  protected function htmlEntitiesNormalizeXml($string) {
    // Get rid of existing entities and double-escape.
    $string = html_entity_decode(stripslashes($string), ENT_QUOTES, 'UTF-8');
    $result = '';
    // Create array of multi-byte characters.
    $ar = preg_split('/(?<!^)(?!$)/u', $string);
    foreach ($ar as $c) {
      $o = ord($c);
      if (
        // Any multi-byte character's length is > 1
        (strlen($c) > 1)
        // Control characters are below 32, latin special chars are above 126.
        // Non-latin characters are above 126 as well, including &nbsp;.
        || ($o < 32 || $o > 126)
        // That's the ampersand.
        || ($o == 38)
        /* This following ranges includes single&double quotes,
         * ampersand, hash sign, less- and greater-than signs.
         * We do not want to replace these, otherwise HTML will break,
         * except for the ampersand which is targeted above.
         * Non-breaking spaces &nbsp; are converted to &#160; .
         */
        #|| ($o > 33 && $o < 40) /* quotes + ampersand */
        #|| ($o > 59 && $o < 63) /* html */
      ) {
        // Convert to numeric entity.
        $c = mb_encode_numericentity($c, [0x0, 0xffff, 0, 0xffff], 'UTF-8');
      }
      $result .= $c;
    }
    // Mask ampersands
    $result = str_replace('&#38;', '###amp###', $result);
    $result = html_entity_decode($result, ENT_HTML5);
    $result = str_replace('###amp###', '&#38;', $result);
    // Escape < characters that are not part of tags or comments.
    $result = preg_replace_callback('/<(?!\/?(?:\w+\s*\/?|\!--))/s', function ($matches) {
      return '&lt;';
    }, $result);

    return $result;
  }

  /**
   * Traverses the DOM tree (recursive).
   * If current DOMNode element has children,
   * this function calls itself.
   * #text nodes never have any children, there
   * Onomasticon filter is applied.
   *
   * @param $dom \DOMNode
   * @param $tree array
   *
   */
  public function processChildren($dom, $tree = []) {
    if ($dom->hasChildNodes()) {
      // Children present, this can't be a #text node.
      // Add the tag name to the tree, so we know the
      // hierarchy.
      $tree[] = $dom->nodeName;
      // Recursive call on first child.
      foreach ($dom->childNodes as $child) {
        $this->processChildren($child, $tree);
      }
    }
    else {
      // No children present => end of tree branch.
      if ($dom->nodeName == '#text' && !$dom->isWhitespaceInElementContent()) {
        // Element is of type #text and has content.

        // First check tree for ancestor tags not allowed.
        $disabledTags = explode(' ', $this->settings['onomasticon_disabled']);
        // Sanitize user input
        $disabledTags = array_map(
          function($tag) { return preg_replace("/[^a-z1-6]*/", "", strtolower(trim($tag))); },
          $disabledTags
        );
        // Add Onomasticon tag and anchor tag.
        $disabledTags[] = $this->settings['onomasticon_tag'];
        $disabledTags[] = 'a';
        $disabledTags[] = 'nonomasticon'; // Required for CKEditor plugin.
        $disabledTags = array_unique($disabledTags);
        // Find the bad boys.
        $badTags = array_intersect($disabledTags, $tree);
        if (count($badTags) == 0) {
          // To avoid double replacements, check if this element
          // has been processed already. DomNodePath is unique.
          if (!in_array($dom->getNodePath(), $this->processedPaths)) {
            // Element has not been processed yet. Let's do this!
            // Original nodeValue (textContent).
            $textOrigin = $dom->nodeValue;
            // Processed text, Onomasticon has been applied.
            $textReplica = $this->replaceTerms($textOrigin);
            // Did the filter find anything?
            if ($textOrigin !== $textReplica) {
              // Indeed, let's save the information for later.
              $this->htmlReplacements[] = [
                'dom' => $dom,
                'html' => $textReplica,
              ];
            }
            // Add element to processed items.
            $this->processedPaths[] = $dom->getNodePath();
          }
        }
      }
    }
  }

  /**
   * This is the actual filter function.
   *
   * @param $text String containing a #text DOMNode value.
   * @return string Processed string.
   */
  public function replaceTerms($text) {
    $pregLimit = $this->settings['onomasticon_repetition'] ? 1 : -1;

    // Cycle through terms and search for occurrence.
    $replacements = [];
    $language = \Drupal::languageManager()
      ->getCurrentLanguage(LanguageInterface::TYPE_CONTENT)
      ->getId();

    $terms = $this->getTaxonomyTerms();
    if (is_array($terms) && count($terms) > 0) {
      foreach ($this->getTaxonomyTerms() as $term) {
        if ($this->settings['onomasticon_repetition'] && array_key_exists($term->id() . ':0', $this->getCacheTerms())) {
          continue;
        }
        if (!$term->hasTranslation($language)) {
          continue;
        }
        $term = $term->getTranslation($language);
        $termNames = [$term->label()];
        if (!empty(\Drupal::hasService('synonyms.provider_service'))) {
          $synonyms = \Drupal::service('synonyms.provider_service')->getEntitySynonyms($term);
          foreach ($synonyms as $synonym) {
            $termNames[] = $synonym;
          }
        }
        foreach ($termNames as $index => $termName) {
          if ($this->settings['onomasticon_ignorecase']) {
            // Get position of term in text independent of case.
            $pos = strpos(strtolower($text), strtolower($termName));
          }
          else {
            // Get position of term in text.
            $pos = strpos($text, $termName);
            // If not found try capitalized (e.g. beginning of sentence).
            if ($pos === FALSE) {
              $pos = strpos($text, ucfirst($termName));
            }
          }

          if ($pos === FALSE) {
            continue;
          }
          // Set the correct cased needle.
          $needles = [substr($text, $pos, strlen($termName))];
          if ($this->settings['onomasticon_ignorecase']) {
            if ($needles[0] === strtolower($needles[0])) {
              $needles[] = ucfirst($needles[0]);
            }
            else {
              $needles[] = strtolower($needles[0]);
            }
          }
          if (!array_key_exists($term->id() . ':' . $index, $this->getCacheTerms())) {
            $this->addCacheTerm($term->id() . ':' . $index);
          }
          $customDefinitionField = $this->settings['onomasticon_definition_field'];
          $fieldName = $term->hasField($customDefinitionField) ? $customDefinitionField : 'description';
          $fieldType = $term->get($fieldName)->getFieldDefinition()->getType();
          $description = $term->get($fieldName)->value;
          if ($this->settings['onomasticon_definition_filters'] && in_array($fieldType, ['text', 'text_with_summary', 'text_long'])) {
            $description = check_markup($description, $term->get($fieldName)->format);
          }

          // Set the implementation method.
          $implement = $this->settings['onomasticon_implement'];
          if ($implement == 'attr_title') {
            $description = strip_tags($description);
            // Title attribute is enclosed in double quotes,
            // we need to escape double quotes in description.
            // TODO: Instead of removing double quotes altogether, escape them.
            $description = str_replace('"', '', $description);
            // Replace no-breaking spaces with normal ones.
            $description = str_replace('&nbsp;', ' ', $description);
            // Trim multiple white-space characters with single space.
            $description = preg_replace('/\s+/m', ' ', $description);
          }

          foreach ($needles as $n => $needle) {
            $onomasticon = [
              '#theme' => 'onomasticon',
              '#tag' => $this->settings['onomasticon_tag'],
              '#needle' => $needle,
              '#description' => $description,
              '#implement' => $implement,
              '#orientation' => $this->settings['onomasticon_orientation'],
              '#cursor' => $this->settings['onomasticon_cursor'],
              '#termlink' => $this->settings['onomasticon_termlink'],
              '#termpath' => $term->toUrl()->toString(),
              '#term' => $term,
            ];

            $placeholder = '###' . $term->id() . ':' . $index . ($n === 1 ? '-alt' : '') . '###';
            $replacements[$placeholder] = trim(\Drupal::service('renderer')
              ->render($onomasticon));

            $count = NULL;
            $text = preg_replace("/(?<![a-zA-Z0-9_äöüÄÖÜ])" . preg_quote($needle, '/') . "(?![a-zA-Z0-9_äöüÄÖÜ])/",
              $placeholder, $text, $pregLimit, $count);
            if ($count === 0) {
              unset($this->termCache[$term->id() . ':' . $index]);
            }
          }
        }
      }

      foreach ($replacements as $placeholder => $replacement) {
        $text = str_replace($placeholder, $replacement, $text, $pregLimit);
      }
    }
    return $text;
  }

  /**
   * Get terms id already processed from cache.
   */
  private function getCacheTerms() {
    // We choose between the local cache and the global cache.
    return $this->settings['onomasticon_repetition'] === 'page' ?
      onomasticon_get_term_cache() :
      $this->termCache;
  }

  /**
   * Register a term id in the cache.
   */
  private function addCacheTerm($tid) {
    // We choose between the local cache and the global cache.
    $this->settings['onomasticon_repetition'] === 'page' ?
      onomasticon_set_term_cache($tid):
      $this->termCache[$tid] = TRUE;
  }

  /**
   * Singleton to retrieve all terms.
   *
   * @return Term[]
   */
  private function getTaxonomyTerms() {
    if (empty($this->terms)) {
      $terms = \Drupal::entityTypeManager()
        ->getStorage('taxonomy_term')
        ->loadByProperties(['vid' => $this->settings['onomasticon_vocabulary'], 'status' => TRUE]);
    }
    \Drupal::moduleHandler()->alter('onomasticon_terms', $terms);
    if (is_array($terms)) {
      $this->terms = $terms;
    }

    return $this->terms;
  }

  /**
   * Filter settings form.
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   * @return array
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $vocabularies = Vocabulary::loadMultiple();
    $options = [];
    foreach ($vocabularies as $vocabulary) {
      $options[$vocabulary->id()] = $vocabulary->get('name');
    }
    $form['onomasticon_vocabulary'] = [
      '#type' => 'select',
      '#title' => $this->t('Vocabulary'),
      '#options' => $options,
      '#default_value' => $this->settings['onomasticon_vocabulary'],
      '#description' => $this->t('Choose the vocabulary that holds the glossary terms.'),
    ];
    $form['onomasticon_definition_field'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Definition'),
      '#default_value' => $this->settings['onomasticon_definition_field'],
      '#description' => $this->t('Enter the machine field name that holds the definition of the word. Ignore if you put your definitions in the default description taxonomy field.'),
    ];
    $form['onomasticon_definition_filters'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Run text filters on term description'),
      '#default_value' => $this->settings['onomasticon_definition_filters'],
      '#description' => $this->t('If checked and the selected description source is a formatted text field, all text filters for the format used will run on the description value (check_markup). <br /><i><b>Caution:</b> This can lead to infinite loops and break your site if term descriptions contain other glossary terms.</i>'),
    ];
    $form['onomasticon_tag'] = [
      '#type' => 'select',
      '#title' => $this->t('HTML tag'),
      '#options' => [
        'dfn' => $this->t('Definition (dfn)'),
        'abbr' => $this->t('Abbreviation (abbr)'),
        'cite' => $this->t('Title of work (cite)'),
      ],
      '#default_value' => $this->settings['onomasticon_tag'],
      '#description' => $this->t('Choose the HTML tag to contain the glossary term.'),
    ];
    $form['onomasticon_disabled'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Disabled tags'),
      '#default_value' => $this->settings['onomasticon_disabled'],
      '#description' => $this->t('Enter all HTML elements in which terms should not be replaced. Anchor tag as well as the default HTML tag are added to that list automatically.'),
    ];
    $form['onomasticon_implement'] = [
      '#type' => 'select',
      '#title' => $this->t('Implementation'),
      '#options' => [
        'extra_element' => $this->t('Extra element'),
        'attr_title' => $this->t('Title attribute'),
        'accessibility_first' => $this->t('Accessibility first'),
      ],
      '#default_value' => $this->settings['onomasticon_implement'],
      '#description' => $this->t('Choose the implementation of the glossary term description. Due to HTML convention, the description will be stripped of any tags as they are not allowed in a tag\'s attribute.'),
    ];
    $form['onomasticon_orientation'] = [
      '#type' => 'select',
      '#title' => $this->t('Orientation'),
      '#options' => [
        'above' => $this->t('Above'),
        'below' => $this->t('Below'),
      ],
      '#default_value' => $this->settings['onomasticon_orientation'],
      '#description' => $this->t('Choose whether the tooltip should appear above or below the hovered glossary term.'),
      '#states' => [
        'visible' => [
          'select[name="filters[filter_onomasticon][settings][onomasticon_implement]"]' => [
            ['value' => 'extra_element'],
            'or',
            ['value' => 'accessibility_first'],
          ],
        ],
      ],
    ];
    $form['onomasticon_cursor'] = [
      '#type' => 'select',
      '#title' => $this->t('Mouse cursor'),
      '#options' => [
        'default' => $this->t('Default (Text cursor)'),
        'help' => $this->t('Help cursor'),
        'none' => $this->t('Hide cursor'),
      ],
      '#default_value' => $this->settings['onomasticon_cursor'],
      '#description' => $this->t('Choose a style the mouse cursor will change to when hovering a glossary term.'),
    ];
    $form['onomasticon_ignorecase'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Ignore case'),
      '#default_value' => $this->settings['onomasticon_ignorecase'],
      '#description' => $this->t('If checked, Onomasticon will find all occurrences of a term regardless of case (even CamelCase will work). If not checked, Onomasticon will only find the exact cased term or with the first letter capitalized (i.e. for start of sentences).'),
    ];
    $form['onomasticon_repetition'] = [
      '#type' => 'select',
      '#title' => $this->t('Repetition'),
      '#options' => [
        '' => $this->t('Add definition to all occurences.'),
        'text' => $this->t('Add definition to first occurrence of term in text, only.'),
        'page' => $this->t('Add definition to first occurence of term in page, only.'),
      ],
      '#default_value' => $this->settings['onomasticon_repetition'],
      '#description' => $this->t('Choose wether to process all occurrences in text, ignore repetition in same text block or ignore repetition in whole page.'),
    ];
    $form['onomasticon_termlink'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Add a link to the term entity.'),
      '#default_value' => $this->settings['onomasticon_termlink'],
      '#description' => $this->t('If you enable this option the tooltip will be extended by a link to the full term entity which enables you to show even more information. This only works if the implementation is done with an extra element.'),
    ];
    return $form;
  }
}
